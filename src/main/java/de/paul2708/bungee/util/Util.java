package de.paul2708.bungee.util;

import de.paul2708.common.abuse.Abuse;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Paul on 08.09.2016.
 */
public class Util {

    // Time
    private static DateFormat format;

    static {
        format = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");
    }

    public static String getTime(long time) {
        return format.format(new Date(time));
    }

    // Abuse
    public static boolean isSameAbuse(Abuse one, Abuse second) {
        if (one.getAbuserUuid().equals(second.getAbuserUuid()) &&
                one.getLastTo() == second.getLastTo() &&
                one.getAction() == second.getAction() &&
                one.getTimeStamp() == second.getTimeStamp()) {
            return true;
        }

        return false;
    }
}
