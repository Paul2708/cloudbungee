package de.paul2708.bungee.util;

/**
 * Created by Paul on 05.03.2017.
 */
public class Settings {

    public static String MOTD = "";
    public static int MAX_PLAYERS = -1;
    public static int TEAM_LEVEL = 100;
    public static boolean JOIN_ABLE = false;
}
