package de.paul2708.bungee.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.bungee.player.Profile;
import de.paul2708.common.packet.key.KeyRedeemedPacket;

/**
 * Created by Paul on 01.01.2017.
 */
public class KeyPacketHandler implements PacketListener {

    @PacketHandler
    public void onKeyRedeemed(Session session, KeyRedeemedPacket packet) {
        // Key
        Profile.getProfile(packet.getUuid()).setKey(true);
    }
}
