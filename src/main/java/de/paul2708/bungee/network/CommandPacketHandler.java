package de.paul2708.bungee.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.bungee.abuse.AbuseManager;
import de.paul2708.bungee.blacklist.BlacklistManager;
import de.paul2708.bungee.maintenance.MaintenanceManager;
import de.paul2708.bungee.util.Settings;
import de.paul2708.common.packet.command.BroadcastPacket;
import de.paul2708.common.packet.other.AbusePacket;
import de.paul2708.common.packet.other.BlacklistPacket;
import de.paul2708.common.packet.other.MaintenancePacket;
import de.paul2708.common.packet.server.MaxPlayerPacket;
import de.paul2708.common.packet.server.MotdPacket;
import net.md_5.bungee.api.ProxyServer;

/**
 * Created by Paul on 08.10.2016.
 */
public class CommandPacketHandler implements PacketListener {

    @PacketHandler
    public void onBroadcastPacket(Session session, BroadcastPacket packet) {
        // Broadcast
        ProxyServer.getInstance().broadcast(" ");
        ProxyServer.getInstance().broadcast(packet.getMessage());
        ProxyServer.getInstance().broadcast(" ");
    }

    @PacketHandler
    public void onMotdPacket(Session session, MotdPacket packet) {
        // Motd
        Settings.MOTD = packet.getMotd();
    }

    @PacketHandler
    public void onMaxCount(Session session, MaxPlayerPacket packet) {
        // Max players
        Settings.MAX_PLAYERS = packet.getMaxPlayers();
    }

    @PacketHandler
    public void onBlacklistPacket(Session session, BlacklistPacket packet) {
        // Blacklist
        if (packet.getOperation().equalsIgnoreCase("add")) {
            BlacklistManager.names.add(packet.getName());
        } else if (packet.getOperation().equalsIgnoreCase("remove")) {
            BlacklistManager.names.remove(packet.getName());
        } else {
            System.out.println("Unknown operation for blacklist");
        }
    }

    @PacketHandler
    public void onMaintenancePacket(Session session, MaintenancePacket packet) {
        // Maintenance
        if (packet.getOperation().equalsIgnoreCase("enable")) {
            MaintenanceManager.enable(packet.getReason());
        } else if (packet.getOperation().equalsIgnoreCase("disable")) {
            MaintenanceManager.disable();
        } else {
            System.out.println("Unknown operation for maintenance");
        }
    }

    @PacketHandler
    public void onAbusePacket(Session session, AbusePacket packet) {
        // Abuse
        if (packet.getAction().equalsIgnoreCase("remove")) {
            AbuseManager.remove(packet.getAbuse());
        } else {
            AbuseManager.abuses.add(packet.getAbuse());
        }
    }
}
