package de.paul2708.bungee.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.bungee.CloudBungee;
import de.paul2708.bungee.player.Profile;
import de.paul2708.bungee.util.Settings;
import de.paul2708.common.packet.player.PlayerChangeServerPacket;
import de.paul2708.common.packet.player.PlayerKickPacket;
import de.paul2708.common.packet.player.PlayerMessagePacket;
import de.paul2708.common.packet.rank.GetPlayerRankPacket;
import de.paul2708.common.packet.rank.RankUpdatePacket;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

/**
 * Created by Paul on 08.10.2016.
 */
public class PlayerPacketHandler implements PacketListener {

    @PacketHandler
    public void onMessagePacket(Session session, PlayerMessagePacket packet) {
        // Message
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(packet.getName());
        if (player != null) {
            String[] messages = packet.getMessages();
            for (String message : messages) {
                player.sendMessage(message);
            }
        }
    }

    @PacketHandler
    public void onChangePacket(Session session, PlayerChangeServerPacket packet) {
        // Server change
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(UUID.fromString(packet.getUuid()));
        ServerInfo info = ProxyServer.getInstance().getServerInfo(packet.getServerName());
        if (info == null) {
            player.sendMessage(CloudBungee.TAG + "§7Der Server §e" + packet.getServerName() + " §7konnte nicht gefunden werden.");
        } else {
            player.connect(info);
        }
    }

    @PacketHandler
    public void onKickPacket(Session session, PlayerKickPacket packet) {
        // Kick
        String reason = ChatColor.translateAlternateColorCodes('&', packet.getReason());
        ProxyServer.getInstance().getPlayer(packet.getName()).
                disconnect(new TextComponent(reason));
    }

    @PacketHandler
    public void onRankRequestPacket(Session session, GetPlayerRankPacket packet) {
        // Rank request
        if (packet.getRank().getPermissionLevel() >= Settings.TEAM_LEVEL) {
            Profile.getProfile(UUID.fromString(packet.getUuid())).setTeamMember(true);
        }
    }

    @PacketHandler
    public void onRankUpdatePacket(Session session, RankUpdatePacket packet) {
        // Rank update
        UUID uuid = UUID.fromString(packet.getUuid());

        // Team
        if (packet.getTo().getPermissionLevel() >= Settings.TEAM_LEVEL) {
            Profile.getProfile(uuid).setTeamMember(true);
        } else {
            Profile.getProfile(uuid).setTeamMember(false);
        }
        // Premium
        if (packet.getTo().getPermissionLevel() >= 20) {
            Profile.getProfile(uuid).setJoin(true);
        } else {
            Profile.getProfile(uuid).setJoin(false);
        }
    }
}
