package de.paul2708.bungee.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.bungee.abuse.AbuseManager;
import de.paul2708.bungee.blacklist.BlacklistManager;
import de.paul2708.bungee.maintenance.MaintenanceManager;
import de.paul2708.bungee.player.Profile;
import de.paul2708.bungee.util.Settings;
import de.paul2708.common.packet.client.CloudStatusPacket;
import de.paul2708.common.packet.server.AddServerPacket;
import de.paul2708.common.packet.server.BungeeRegistrationPacket;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * Created by Paul on 08.10.2016.
 */
public class ServerPacketHandler implements PacketListener {

    @PacketHandler
    public void onCloudPacket(Session session, CloudStatusPacket packet) {
        // Status
        Settings.JOIN_ABLE = packet.getStatus().equalsIgnoreCase("started");
    }

    @PacketHandler
    public void onRegistrationPacket(Session session, BungeeRegistrationPacket packet) {
        Settings.MOTD = packet.getMotd();
        Settings.MAX_PLAYERS = packet.getMaxPlayers();
        Settings.TEAM_LEVEL = packet.getTeamLevel();

        BlacklistManager.resolve(packet.getBlackList());

        if (packet.isMaintenance()) {
            MaintenanceManager.enable(packet.getMaintenanceReason());
        }

        for (UUID teamMember : packet.getTeamMember()) {
            Profile.getProfile(teamMember).setTeamMember(true);
        }
        for (UUID teamMember : packet.getJoinMember()) {
            Profile.getProfile(teamMember).setJoin(true);
        }

        AbuseManager.abuses.addAll(packet.getAbuses());
    }

    @PacketHandler
    public void onServerPacket(Session session, AddServerPacket packet) {
        // Add server
        InetSocketAddress address = new InetSocketAddress(packet.getIp(), packet.getPort());
        ServerInfo info = ProxyServer.getInstance().constructServerInfo(packet.getName(), address, "", false);
        ProxyServer.getInstance().getServers().put(packet.getName(), info);
    }

}
