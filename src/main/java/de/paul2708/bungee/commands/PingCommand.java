package de.paul2708.bungee.commands;

import de.paul2708.bungee.CloudBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Paul on 07.08.2016.
 */
public class PingCommand extends Command {

    public PingCommand() {
        super("ping");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage("Only Player!");
            return;
        }

        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        proxiedPlayer.sendMessage(CloudBungee.TAG + "§7Dein Ping: §e" + proxiedPlayer.getPing() + " ms");
    }
}
