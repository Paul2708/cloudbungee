package de.paul2708.bungee.commands;

import de.paul2708.bungee.CloudBungee;
import de.paul2708.common.packet.command.CommandPacket;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Paul on 11.08.2016.
 */
public class BasicCommand extends Command {

    private String command;

    public BasicCommand(String command) {
        super(command);

        this.command = command;
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage("[Cloud] Only player can use cloud commands!");
            return;
        }

        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        CloudBungee.getClient().send(new CommandPacket(proxiedPlayer.getUniqueId().toString(), command, strings));
    }
}
