package de.paul2708.bungee;

import de.paul2708.bungee.commands.BasicCommand;
import de.paul2708.bungee.commands.PingCommand;
import de.paul2708.bungee.file.AddressesFile;
import de.paul2708.bungee.listener.*;
import de.paul2708.bungee.network.CommandPacketHandler;
import de.paul2708.bungee.network.KeyPacketHandler;
import de.paul2708.bungee.network.PlayerPacketHandler;
import de.paul2708.bungee.network.ServerPacketHandler;
import de.paul2708.common.command.CommandType;
import de.paul2708.common.network.PacketClient;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.client.ClientLoginPacket;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

/**
 * Created by Paul on 07.08.2016.
 */
public class CloudBungee extends Plugin {

    // TODO: chat in bungee?

    public static final String TAG = "§cSystem§8│ §r";

    @Getter
    private static CloudBungee instance;

    @Getter
    private static PacketClient client;

    @Override
    public void onEnable() {
        CloudBungee.instance = this;

        // File
        AddressesFile addressesFile = new AddressesFile();

        // Commands
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerCommand(this, new PingCommand());

        for (CommandType type : CommandType.values()) {
            BasicCommand command = new BasicCommand(type.getCommand());
            pluginManager.registerCommand(this, command);
        }

        // Listener
        pluginManager.registerListener(this, new LoginListener());
        pluginManager.registerListener(this, new PostLoginListener());
        pluginManager.registerListener(this, new DisconnectListener());
        pluginManager.registerListener(this, new ProxyPingListener());
        pluginManager.registerListener(this, new TabCompleteListener());
        pluginManager.registerListener(this, new ServerConnectedListener());
        pluginManager.registerListener(this, new ChatListener());

        // Client
        CloudBungee.client = new PacketClient(addressesFile.getPacketIp(), addressesFile.getPort());
        client.registerListeners(new PlayerPacketHandler(), new ServerPacketHandler(),
                new CommandPacketHandler(), new KeyPacketHandler());
        client.connect();
        client.send(new ClientLoginPacket(ClientType.BUNGEE.getId(), "BungeeCord"));
    }

    @Override
    public void onDisable() { }
}
