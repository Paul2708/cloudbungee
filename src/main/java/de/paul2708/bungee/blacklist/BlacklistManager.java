package de.paul2708.bungee.blacklist;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 06.09.2016.
 */
public class BlacklistManager {

    public static List<String> players = new CopyOnWriteArrayList<>();

    public static List<String> names = new CopyOnWriteArrayList<>();

    public static void resolve(List<String> list) {
        list.forEach(name -> names.add(name));
    }

    public static boolean contains(String name) {
        for (String all : names) {
            if (all.equalsIgnoreCase(name)) {
                return true;
            }
        }

        return false;
    }
}
