package de.paul2708.bungee.listener;

import de.paul2708.bungee.CloudBungee;
import de.paul2708.bungee.blacklist.BlacklistManager;
import de.paul2708.common.packet.player.PlayerLogoutPacket;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Paul on 07.08.2016.
 */
public class DisconnectListener implements Listener {

    @EventHandler
    public void onDisconnect(PlayerDisconnectEvent e) {
        ProxiedPlayer player = e.getPlayer();

        if (BlacklistManager.players.contains(player.getName())) {
            BlacklistManager.players.remove(player.getName());
            return;
        }

        CloudBungee.getClient().send(new PlayerLogoutPacket(player.getUniqueId().toString()));
    }
}
