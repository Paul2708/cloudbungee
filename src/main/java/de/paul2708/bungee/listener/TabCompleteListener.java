package de.paul2708.bungee.listener;

import de.paul2708.common.command.CommandType;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Paul on 05.09.2016.
 */
public class TabCompleteListener implements Listener {

    @EventHandler
    public void onComplete(TabCompleteEvent e) {
        String tab = e.getCursor();

        if (!tab.startsWith("/")) return;
        String commandStart = tab.substring(1);

        if (commandStart.equalsIgnoreCase("")) {
            for (CommandType type : CommandType.values()) {
                e.getSuggestions().add("/" + type.getCommand());
            }
        } else {
            for (CommandType type : CommandType.values()) {
                if (type.getCommand().startsWith(commandStart.toLowerCase()))  {
                    e.getSuggestions().add("/" + type.getCommand());
                }
            }
        }
    }
}
