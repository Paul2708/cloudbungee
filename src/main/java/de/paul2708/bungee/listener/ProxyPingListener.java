package de.paul2708.bungee.listener;

import de.paul2708.bungee.maintenance.MaintenanceManager;
import de.paul2708.bungee.util.Settings;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Paul on 02.09.2016.
 */
public class ProxyPingListener implements Listener {

    @EventHandler
    public void onPing(ProxyPingEvent e) {
        ServerPing response = e.getResponse();
        if (Settings.MOTD == null || Settings.MAX_PLAYERS == -1) {
            return;
        }

        if (!Settings.MOTD.equalsIgnoreCase("")) {
            String motd = ChatColor.translateAlternateColorCodes('&', Settings.MOTD);
            response.setDescription(motd.replace("\\n", "\n"));
        }

        ServerPing.Players p = new ServerPing.Players(Settings.MAX_PLAYERS, response.getPlayers().getOnline(), response.getPlayers().getSample());
        response.setPlayers(p);
        if (MaintenanceManager.isEnabled()) {
            response.setVersion(new ServerPing.Protocol("Wartungsarbeiten", response.getVersion().getProtocol() -1));
            response.setDescription("§bOzeanGames.net §7- §6Minigames Netzwerk §8[§71.8.x-1.11.x§8] \n                        §6♦♦ §cWartungsarbeiten §6♦♦");
        }

        e.setResponse(response);
    }
}
