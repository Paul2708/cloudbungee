package de.paul2708.bungee.listener;

import de.paul2708.bungee.CloudBungee;
import de.paul2708.common.packet.player.PlayerChangeServerPacket;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Paul on 17.08.2016.
 */
public class ServerConnectedListener implements Listener {

    @EventHandler
    public void onConnected(ServerConnectedEvent e) {
        ProxiedPlayer player = e.getPlayer();
        String name = e.getServer().getInfo().getName();

        player.setTabHeader(TextComponent.fromLegacyText("§bOzeanGames.net §8» §eNetzwerk"),
                TextComponent.fromLegacyText("§6Du befindest dich auf §e" + e.getServer().getInfo().getName() + "\n§cSpieler reporten mit /report §e<Spieler> \n\n§cWebseite: §7OzeanGames.net §7- §cTs3: §7OzeanGames.net"));

        CloudBungee.getClient().send(
                new PlayerChangeServerPacket(player.getUniqueId().toString(), name));
    }
}
