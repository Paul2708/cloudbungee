package de.paul2708.bungee.listener;

import de.paul2708.bungee.CloudBungee;
import de.paul2708.bungee.player.Profile;
import de.paul2708.common.packet.player.PlayerLoginPacket;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Paul on 07.08.2016.
 */
public class PostLoginListener implements Listener {

    @EventHandler
    public void onLogin(PostLoginEvent e) {
        ProxiedPlayer player = e.getPlayer();

        new Profile(player.getUniqueId());

        CloudBungee.getClient().send(new PlayerLoginPacket(player.getName(),
                player.getUniqueId().toString(), player.getAddress().getAddress().getHostAddress()));
    }
}
