package de.paul2708.bungee.listener;

import de.paul2708.bungee.abuse.AbuseManager;
import de.paul2708.bungee.blacklist.BlacklistManager;
import de.paul2708.bungee.maintenance.MaintenanceManager;
import de.paul2708.bungee.player.Profile;
import de.paul2708.bungee.util.Settings;
import de.paul2708.bungee.util.Util;
import de.paul2708.common.abuse.Abuse;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

/**
 * Created by Paul on 07.08.2016.
 */
public class LoginListener implements Listener {

    private long delay = System.currentTimeMillis();

    private int count = 0;

    @EventHandler
    public void onLogin(LoginEvent e) {
        String name = e.getConnection().getName();
        UUID uuid = e.getConnection().getUniqueId();

        // Ban
        Abuse abuse = AbuseManager.isBanned(e.getConnection().getUniqueId());
        if (abuse != null) {
            if(abuse.getLastTo() == -1) {
                e.setCancelReason("§cDu bist §apermanent §cvom OzeanGames.net Netzwerk gebannt! \n§cGrund: §a" + abuse.getReason() + "\n\n§6Du kannst einen Entbannungsantrag auf §aOzeanGames.net §6stellen!");
            } else {
                e.setCancelReason("§cDu bist bis zum §a" + Util.getTime(abuse.getLastTo()) +" §cvom OzeanGames.net Netzwerk gebannt! \n§cGrund: §a" + abuse.getReason() + "\n\n§6Du kannst einen Entbannungsantrag auf §aOzeanGames.net §6stellen!");
            }
            e.setCancelled(true);
            return;
        }

        // Maintenance
        if (MaintenanceManager.isEnabled()) {
            if (!Profile.getProfile(uuid).isTeamMember()) {
                e.setCancelReason(MaintenanceManager.getReason());
                e.setCancelled(true);
                return;
            }
        }

        // Blacklist
        if (BlacklistManager.contains(name)) {
            BlacklistManager.players.add(name);
            e.setCancelReason("§cDu stehst auf der Blacklist. " +
                    "\n\n§cMelde dich im TS für weitere Infos.");
            e.setCancelled(true);
            return;
        }

        // Key
       /* if (!Profile.getProfile(uuid).isTeamMember()) {
            if (!Profile.getProfile(uuid).hasKey()) {
                e.setCancelReason("§cDu musst deinen Beta-Key einlösen auf:\n§eozeangames.net/betakey");
                e.setCancelled(true);
                return;
            }
        } */

        // Full
        if (ProxyServer.getInstance().getOnlineCount() >= Settings.MAX_PLAYERS) {
            if (!Profile.getProfile(uuid).canJoin() && !Profile.getProfile(uuid).isTeamMember()) {
                e.setCancelReason("§cDas Netzwerk ist voll.\n§cKaufe dir Premium um joinen zu können.");
                e.setCancelled(true);
                return;
            }
        }

        // Timing
        count++;

        if (System.currentTimeMillis() - delay >= 1000) {
            if (count >= 5) {
                e.setCancelReason("§cEs melden sich gerade zu viele Spieler an.\n§cVersuche es in paar Sekunden nochmal.");
                e.setCancelled(true);
                return;
            }

            delay = System.currentTimeMillis();
            count = 0;
        }

        // Cloud started
        if (!Settings.JOIN_ABLE) {
            e.setCancelReason("§cDer CloudServer ist nicht erreichbar! \n\n §cWarte einen Moment");
            e.setCancelled(true);
        }
    }
}
