package de.paul2708.bungee.listener;

import de.paul2708.bungee.CloudBungee;
import de.paul2708.bungee.abuse.AbuseManager;
import de.paul2708.bungee.util.Util;
import de.paul2708.common.abuse.Abuse;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Paul on 02.10.2016.
 */
public class ChatListener implements Listener {

    @EventHandler
    public void onChat(ChatEvent e) {

        if (e.getSender() instanceof ProxiedPlayer) {
            if (e.getMessage().startsWith("/")) return;

            ProxiedPlayer player = (ProxiedPlayer) e.getSender();
            Abuse abuse = AbuseManager.isMuted(player);
            if (abuse != null) {
                player.sendMessage(CloudBungee.TAG + "§cDu bist gemutet.");
                player.sendMessage(CloudBungee.TAG + "Grund: " + abuse.getReason());
                player.sendMessage(CloudBungee.TAG + "Ende: " + (abuse.getLastTo() == -1 ? "permanent" :
                        Util.getTime(abuse.getLastTo())));

                e.setCancelled(true);
            }
        }
    }
}
