package de.paul2708.bungee.abuse;

import de.paul2708.bungee.util.Util;
import de.paul2708.common.abuse.Abuse;
import de.paul2708.common.abuse.AbuseAction;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 02.10.2016.
 */
public class AbuseManager {

    public static List<Abuse> abuses = new CopyOnWriteArrayList<>();

    public static void remove(Abuse abuse) {
        abuses.stream().filter(all -> Util.isSameAbuse(all, abuse)).forEach(all -> abuses.remove(all));
    }

    public static Abuse isBanned(UUID uuid) {
        for (Abuse abuse : abuses) {
            if (abuse.getAction() == AbuseAction.BAN &&
                    abuse.getAbuserUuid().equals(uuid)) {
                return abuse;
            }
        }

        return null;
    }

    public static Abuse isMuted(ProxiedPlayer player) {
        for (Abuse abuse : abuses) {
            if (abuse.getAction() == AbuseAction.MUTE &&
                    abuse.getAbuserUuid().equals(player.getUniqueId())) {
                return abuse;
            }
        }

        return null;
    }
}
