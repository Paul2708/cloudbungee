package de.paul2708.bungee.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 14.10.2016.
 */
public class AddressesFile extends BasicFile {

    public AddressesFile() {
        super("plugins/CloudBungee/addresses.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("default cloud bungee config");

        set("packet_ip", "localhost");
        set("packet_port", 2000);

        save();
    }

    public String getPacketIp() {
        return get("packet_ip");
    }

    public int getPort() {
        return get("packet_port");
    }
}
