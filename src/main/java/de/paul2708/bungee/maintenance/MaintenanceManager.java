package de.paul2708.bungee.maintenance;

/**
 * Created by Paul on 07.09.2016.
 */
public class MaintenanceManager {

    private static boolean enabled = false;
    private static String reason = "§eDas Netzwerk wird gewartet.\n\n§7Mehr Infos auf unserem TS3.";

    public static void enable(String reason) {
        MaintenanceManager.enabled = true;

        if (reason.equalsIgnoreCase("none")) {
            MaintenanceManager.reason = "§eDas Netwerk wird gewartet.\n\n§7Mehr Infos auf unserem TS3.";
        } else {
            MaintenanceManager.reason = reason;
        }
    }

    public static void disable() {
        MaintenanceManager.enabled = false;

        reason = "§eDas Netzwerk wird gewartet.\n\n§7Mehr Infos auf unserem TS3.";
    }

    public static boolean isEnabled() {
        return enabled;
    }

    public static String getReason() {
        return reason;
    }
}
