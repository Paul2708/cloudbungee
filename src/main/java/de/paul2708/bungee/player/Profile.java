package de.paul2708.bungee.player;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 05.03.2017.
 */
public class Profile {

    private static List<Profile> cache = new CopyOnWriteArrayList<>();

    private UUID uuid;

    private boolean teamMember;
    private boolean canJoin;
    private boolean hasKey;

    public Profile(UUID uuid) {
        this.uuid = uuid;

        this.teamMember = false;
        this.canJoin = false;
        this.hasKey = false;

        Profile.cache.add(this);
    }

    public void setTeamMember(boolean isTeam) {
        this.teamMember = isTeam;
    }

    public void setJoin(boolean canJoin) {
        this.canJoin = canJoin;
    }

    public void setKey(boolean hasKey) {
        this.hasKey = hasKey;
    }

    public boolean isTeamMember() {
        return teamMember;
    }

    public boolean canJoin() {
        return canJoin;
    }

    public boolean hasKey() {
        return hasKey;
    }

    public UUID getUuid() {
        return uuid;
    }

    public static Profile getProfile(UUID uuid) {
        for (Profile all : Profile.cache) {
            if (all.getUuid().equals(uuid)) {
                return all;
            }
        }

        System.out.println("Profile is null: Creating new...");
        return new Profile(uuid);
    }
}
